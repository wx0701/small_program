// pages/center/center.js
Page({
  data: {},
  onShow: function () {
    // 页面显示
    var that = this;
    var searchData = wx.getStorageSync('searchData');
    if (searchData) { wx.removeStorageSync('searchData') };
    that.getSystemInfo();
    that.setData({ isMask: true })
    wx.getUserInfo({
      success: function (res) {
        that.setData({
          avatarUrl: res.userInfo.avatarUrl,
        })
      }
    })
  },
  getSystemInfo: function () {
    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          screenheight: (res.windowHeight) * 2 + 'rpx',
          screenwidth: (res.windowWidth) * 2 + 'rpx'
        })
      }
    })
  },
  myAddrListFn: function (e) {
    var that = this;
    that.setData({ isMask: false })
    wx.navigateTo({
      url: '../addrOpt/addrOpt?management=' + true,
    })
  },
  myOrderListFn: function () {
    var that = this;
    that.setData({ isMask: false })
    wx.navigateTo({
      url: '../orderList/orderList',
    })
  },
  myCouponlistFn: function (event) {
    var that = this;
    that.setData({ isMask: false })
    wx.navigateTo({
      url: '../coupons/coupons?info=' + 'coupon',
    })
  },
  onShareAppMessage: function () {

  }
})