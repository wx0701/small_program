// pages/index/index.js
var gConfig = getApp();
var util = require('../../utils/md5.js');
Page({
  data: {
    indicatorDots: true,
    autoplay: true,
    interval: 3000,
    duration: 1000,
    swiperCurrent: 0,
    circular: true,
    search: false,
    hidden: true,
    isSearch: true,
    iscome: true,
    hideloading: true,
    iscoupon: true,
    dot: false,
    imgPath: gConfig.imgHttp,
    bannerImg : '',
    isNotsell: true
  },
  onLoad: function () {
    this.imgUrls();
    this.shopFn();
    this.getPositionFn();
  },
  onShow: function () {
    // 页面显示
    var that = this;
    that.setData({ dot: false, iscome: true, dotclass: ['on', '', ''], isMask: true });
    var searchData = wx.getStorageSync('searchData')
    var sellData = wx.getStorageSync('sellData')
    if (sellData) { that.skipFn(); that.shopFn(); }//如果已经定位  那么就不需要再次定位  直接加载商品数据
    if (that.data.kl && !that.data.shopsData) { that.skipFn(); that.shopFn(); }//防止用户进入小程序至一半时退出后没有数据
    that.getSystemInfo();
  },
  getSystemInfo: function () {
    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          screenheight: (res.windowHeight) * 2 + 'rpx',
          screenwidth: (res.windowWidth) * 2 + 'rpx'
        })
      }
    })
  },
  onHide: function () {
    // 页面隐藏
  },
  onUnload: function () {
    // 页面关闭
  },
  // 轮播图
  imgUrls: function(event){
    var that = this;
    var wxData = wx.getStorageSync('wxData');
    var companyId = wxData.companyId;
    var banners = '';
    var sign = util.hexMD5('companyId=' + gConfig.companyId + gConfig.key);
    wx.request({
      url: gConfig.http + 'xcx/company/info',
      header: {
        'content-type': 'application/json'
      },
      method: "get",
      data: {
        companyId: gConfig.companyId,
        sign: sign
      },
      success: function (res){
        banners = [];
        for (var i = 0; i < res.data.data.banners.length; i++){
          banners.push(gConfig.imgHttp + res.data.data.banners[i])
        }
        that.setData({bannerImg: banners})
        if (banners.length <= 1) {
          that.setData({indicatorDots: false})
        }
      }
    })
  },
  shopFn: function (event) {
    var that = this;
    var wxData = wx.getStorageSync('wxData');
    var companyId = wxData.companyId;
    var sign = util.hexMD5('companyId=' + gConfig.companyId + gConfig.key);
    wx.request({
      url: gConfig.http + 'xcx/company/info',
      data: {
        companyId: gConfig.companyId,
        sign: sign
      },
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        that.setData({
          title: res.data.data.name
        })
        wx.setNavigationBarTitle({ title: res.data.data.name + '官方旗舰店' })
      }
    })
  },
  //获取地理位置
  getPositionFn: function () {
    var that = this;
    wx.getLocation({
      type: 'wgs84',
      success: function (res) {
        that.setData({ isPosition: '' })
        that.seatFn(res.latitude, res.longitude)
      },
      fail: function () {
        that.setData({ isPosition: true })
      }
    })
  },
  //此方法是点击授权的方法
  getaddressFn: function (event) {
    var that = this;
    that.setData({ isPosition: true })
    if (wx.openSetting) {
      wx.openSetting({
        success: (res) => {
          if (res.authSetting["scope.userLocation"] == true) {
            that.setData({ isPosition: '' })
            wx.getLocation({
              scope: "scope.userLocation",
              type: 'wgs84',
              success: function (res) {
                that.seatFn(res.latitude, res.longitude)
              },
              fail: function (res) {
                that.getPositionFn();
              }
            })
          } else {
            that.setData({
              isPosition: true
            })
          }
        }
      })
    } else {
      wx.showModal({
        title: '提示',
        content: '当前微信版本过低，无法使用该功能，请升级到最新微信版本后重试。'
      })
    }

  },
  seatFn: function (lati, longi) {
    //获取当前所在区域
    var that = this;
    that.setData({ kl: 1 })
    var wxData = wx.getStorageSync('wxData');
    var companyId = wxData.companyId;
    var sign = util.hexMD5('x=' + lati + '&y=' + longi + gConfig.key);
    wx.request({
      url: gConfig.http + 'xcx/common/region',
      data: {
        x: lati,
        y: longi,
        sign: sign
      },
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        that.setData({
          region: res.data.data.region
        });
        wx.setStorageSync('sellData', { region: that.data.region, companyId: companyId, regionName: res.data.data.fullName })
        var region = (that.data.region).slice(0, 2)
        that.skipFn();
      },
    })
  },
  //此方法为了防止app.js加载慢于index.js页面
  skipFn: function (event) {
    var that = this;
    var wxData = wx.getStorageSync('wxData');
    var sellData = wx.getStorageSync('sellData')
    if (!wxData) {
      that.setData({ hideloading: false })
      var timer = setInterval(function () {
        var wxData = wx.getStorageSync('wxData');
        if (wxData) {
          that.setData({ hideloading: true })
          clearTimeout(timer);
          // if (wxData.clientId == 0) {
          //   // wx.redirectTo({
          //   //   // url: '../register/register'
          //   //   url: '../index/index'
          //   // })
          // } else {
            that.couponFn()
            that.searchsFn();
          // }
        }
      },10)
    } else {
      // if (wxData.clientId == 0) {
      //   // wx.redirectTo({
      //   //   // url: '../register/register'
      //   //   url: '../index/index'
      //   // })
      // } else {
        that.couponFn();
        that.searchsFn();
      // }
    }
  },
  //判断搜索框是否有值
  searchsFn: function (event) {
    var that = this;
    var searchData = wx.getStorageSync('searchData');
    if (searchData) {
      that.searchFn();
    } else {
      that.refreshFn();
    }
  },
  //初始全部商品列表
  refreshFn: function (region) {
    var that = this;
    that.setData({
      tap: 1,
      search: false,
      isSearch: true
    })
    that.shopjoggleFn();

  },
  //当搜索框中的value不为空
  searchFn: function (region) {
    var that = this;
    that.setData({
      search: true,
      isSearch: true,
      tap: 1,
    })
    if (that.data.searchId) {
      that.searchingFn();
    } else {
      that.searchDataFn()
    }
  },
  // 优惠劵方法
  couponFn: function (event) {
    var that = this;
    var wxData = wx.getStorageSync('wxData');
    var companyId = wxData.companyId;
    var searchname = that.data.searchname;
    if (wxData.region == 0) {
      var region = that.data.region;
    } else {
      var region = wxData.region;
    }
    var sign = util.hexMD5('companyId=' + companyId + '&region=' + region + gConfig.key);
    wx.request({
      url: gConfig.http + 'xcx/v2/coupon/list',
      data: {
        'companyId': companyId,
        'region': region,
        'sign': sign
      },
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        var couponData = res.data.data
        if (couponData.length > 0) {
          var discount;
          for (var i = 0; i < couponData.length; i++) {
            couponData[i].endTime = couponData[i].endTime.slice(0, 10)
            discount = (couponData[i].discount).toString().length;
            couponData[i].font = discount;
          }
          var width = (505 * couponData.length) + 'rpx';
          that.setData({
            iscoupon: false,
            coupon: couponData,
            width: width,
          })
        } else {
          that.setData({
            iscoupon: true
          })
        }
      },
      faild: function (res) {
        that.setData({
          iscoupon: true
        })
      }
    })
  },
  // drawFn: function (event) {
  //   var that = this;
  //   var coupon = that.data.coupon;
  //   var cartId = event.currentTarget.dataset.cartId;
  //   var wxData = wx.getStorageSync('wxData')
  //   var id = event.currentTarget.dataset.id;
  //   if (coupon.length > 0) {
  //     for (var i = 0; i < coupon.length; i++) {
  //       if (coupon[i].id == id) {
  //         var sign = util.hexMD5('clientId=' + wxData.clientId + '&companyId=' + coupon[i].companyId + '&couponId=' + id + gConfig.key);
  //         wx.request({
  //           url: gConfig.http + 'xcx/coupon/ledcoupon',
  //           data: {
  //             'clientId': wxData.clientId,
  //             'companyId': coupon[i].companyId,
  //             'couponId': id,
  //             'sign': sign
  //           },
  //           header: {
  //             'content-type': 'application/json'
  //           },
  //           success: function (res) {
  //             var result = res.data.result;
  //             if (result.code == -1) {
  //               wx.showToast({
  //                 title: result.message,
  //                 icon: 'success',
  //                 duration: 500
  //               })
  //             } else {
  //               wx.showToast({
  //                 title: result.message,
  //                 icon: 'success',
  //                 duration: 500
  //               })
  //             }
  //           }
  //         })

  //       }
  //     }
  //   }
  // },
  //搜索框中有内容时当搜索框有焦点时的方法
  searchshopFn: function (event) {
    var that = this;
    var searchname = that.data.searchname;
    wx.navigateTo({
      url: '/pages/search/search?judge=' + true + '&name=' + searchname + '&searchId=' + that.data.searchId
    })
  },
  //上拉加载更多
  onReachBottom: function (event) {
    var that = this
    if (that.data.dot == false){
      if (that.data.searchname) {
        if (that.data.searchId) {
          that.dotfor();
          if (that.data.seachshopData) {
            that.setData({ dot: false, iscome: false })
            setTimeout(function () { that.searchingFn() }, 500)
          }
        } else {
          that.dotfor();
          if (that.data.seachshopData) {
            that.setData({ dot: true, iscome: false })
            setTimeout(function () { that.searchDataFn() }, 500)
          }

        }
      } else {
        that.dotfor();
        that.setData({ dot: false, iscome: false })
        setTimeout(function () { that.shopjoggleFn() }, 500)
      }
    }
    
  },
  //搜索框无内容时上拉加载更多  
  shopjoggleFn: function (event) {
    var that = this;
    var wxData = wx.getStorageSync('wxData');
    var companyId = wxData.companyId;
    var tap = that.data.tap;
    var searchname = that.data.searchname;
    if (wxData.region == 0) {
      var region = that.data.region;
    } else {
      var region = wxData.region;
    }
    var sign = util.hexMD5('companyId=' + companyId + '&pageNum=' + tap + '&perpage=' + 10 + '&region=' + region + gConfig.key);
    wx.request({
      url: gConfig.http + 'xcx/company/items',
      data: {
        companyId: companyId,
        pageNum: tap,
        perpage: 10,
        region: region,
        sign: sign
      },
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        var shopDetailData = res.data.data.list;
        var images = [];
        for (var i = 0; i < shopDetailData.length; i++) {
          shopDetailData[i].price = shopDetailData[i].price.toFixed(2)
          
          shopDetailData[i].defaultImg = shopDetailData[i].defaultImg + '.220x220.' + shopDetailData[i].defaultImg.split('.')[1] 
        }
        that.setData({
          images: images
        })
        if (shopDetailData.length > 0) {

          //将已有的数据和加载的数据放到一起
          if (that.data.tap == 1) {
            that.setData({
              shopsData: shopDetailData,
              tap: tap + 1
            })
          } else {
            var shopsData = that.data.shopsData.concat(shopDetailData)
            //再次进行页面重绘
            that.setData({
              shopsData: shopsData,
              tap: tap + 1
            })
          }
        } else {
          
          that.setData({ isNotsell: false })
          
          if (tap == 1) {
            that.setData({ dot: true, iscome: true })
            // setTimeout(function () {  }, 500)
          } else {

            that.setData({ iscome: false })
            // setTimeout(function () {  }, 3000)
            that.setData({ dot: true })
          }

        }
      }
    })
  },
  //搜索框有内容时且是按照分类搜索时
  searchingFn: function (event) {
    var that = this;
    var tap = that.data.tap;
    var wxData = wx.getStorageSync('wxData');
    var companyId = wxData.companyId;
    var searchname = that.data.searchId;
    if (wxData.region == 0) {
      var region = that.data.region;
    } else {
      var region = wxData.region;
    }
    wx.request({
      url: gConfig.http + 'xcx/company/search',
      data: {
        companyCategoryId: searchname,
        companyId: companyId,
        region: region,
        pageNum: tap,
        perpage: 10
      },
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        var searchcode = res.data.result;
        if (searchcode.code == 200) {
          var searchDetailData = res.data.data.list;
          if (searchDetailData.length > 0) {
            that.setData({ iscome: false,})
            //将已有的数据和加载的数据放到一起
            for (var i = 0; i < searchDetailData.length; i++) {
              searchDetailData[i].price = searchDetailData[i].price.toFixed(2)
            }
            if (that.data.tap == 1) {
              that.setData({
                seachshopData: searchDetailData,
                tap: tap + 1,
                isSearch: true,
              })
            } else {
              var shopsData = (that.data.seachshopData).concat(searchDetailData)
              //再次进行页面重绘
              that.setData({
                seachshopData: shopsData,
                tap: tap + 1,
              })
            }
          }
        } else {
          if (tap == 1) {
            that.setData({
              seachshopData: '',
              isSearch: false,
            })
          } else {
            that.setData({ dot: true, iscome: false })
            // that.setData({ dot: true })
            // that.setData({ iscome: true })
            // setTimeout(function () {  }, 1500)
          }
        }
      }
    })
  },
  //搜索框中有内容时上拉加载更多
  searchDataFn: function () {
    var that = this;
    var tap = that.data.tap;
    var wxData = wx.getStorageSync('wxData');
    var companyId = wxData.companyId;
    var searchname = that.data.searchname;
    if (wxData.region == 0) {
      var region = that.data.region;
    } else {
      var region = wxData.region;
    }
    wx.request({
      url: gConfig.http + 'xcx/company/search',
      data: {
        companyId: companyId,
        region: region,
        keywords: searchname,
        pageNum: tap,
        perpage: 10
      },
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        console.log(res);
        var searchcode = res.data.result;
        if (searchcode.code == 200) {
          var searchDetailData = res.data.data.list;
          if (searchDetailData.length > 0) {
            that.setData({ iscome: false , dot: true})
            //将已有的数据和加载的数据放到一起
            for (var i = 0; i < searchDetailData.length; i++) {
              searchDetailData[i].price = searchDetailData[i].price.toFixed(2)
            }
            console.log(that.data.tap)
            if (that.data.tap == 1) {
              that.setData({
                seachshopData: searchDetailData,
                tap: tap + 1,
                isSearch: true,
                dot: true
              })
            } else {
              var shopsData = (that.data.seachshopData).concat(searchDetailData)
              //再次进行页面重绘
              that.setData({
                seachshopData: shopsData,
                tap: tap + 1,
                dot: true
              })
            }
          }
        } else if (searchcode.code == -1) {
          if (tap == 1) {
            that.setData({
              seachshopData: '',
              isSearch: false
            })
          } else {
            that.setData({ dot: false, iscome: false })
            that.setData({ dot: true })
            setTimeout(function () { that.setData({ iscome: true }) }, 1500)
          }
        }
      }
    })
  },
  //判断搜索框内容有无时所选中的商品
  shopdetailFn: function (event) {
    var that = this;
    that.setData({
      isMask: false
    })
    var wxData = wx.getStorageSync('wxData')
    var searchData = wx.getStorageSync('searchData')
    var searchname = that.data.searchname;
    if (wxData.region == 0) {
      var region = that.data.region;
    } else {
      var region = wxData.region;
    }
    var shopData = that.data.shopsData;
    var index = parseInt(event.currentTarget.dataset.index);
    var seachshopData = that.data.seachshopData;
    var searchname = that.data.searchname;
    var itemId;
    if (searchData) {
      for (var i = 0; i < seachshopData.length; i++) {
        if (index == i) {
          itemId = seachshopData[i].id
          if (seachshopData[i].price == 0) {
            wx.showToast({
              title: '抱歉，该商品暂不在您所在区域销售！',
              icon: 'success',
              duration: 2000,
            })
          } else {
            wx.navigateTo({
              url: '../shopDetail/shopDetail?itemId=' + itemId + '&region=' + region,
            })
          }
        }
      }
    } else {
      for (var i = 0; i < shopData.length; i++) {
        if (index == i) {
          itemId = shopData[i].id
          if (shopData[i].price == 0) {
            that.setData({
              isMask: true
            })
            wx.showToast({
              title: '抱歉，该商品暂不在您所在区域销售！',
              icon: 'success',
              duration: 2000
            })
          } else {
            wx.navigateTo({
              url: '../shopDetail/shopDetail?itemId=' + itemId + '&region=' + region + '&fullregion=' + that.data.region,
            })
          }
        }
      }
    }
    that.setData({
      dot: false,
    })
  },
  focusFn: function (event) {
    var that = this;
    var searchname = that.data.searchname;
    that.setData({ dot: false })
    wx.navigateTo({
      url: '../search/search?region=' + that.data.region
    })
  },
  dotfor: function () {
    var that = this;
    that.setData({ dotclass: ['on', '', ''] });
    var dotclass = that.data.dotclass;
    var n = 1;
    var timer = setInterval(function () {
      n = n > dotclass.length ? 1 : n;
      for (var i = 0; i < dotclass.length; i++) {
        if ((n - 1) == i) {
          dotclass[i] = 'on'
          that.setData({ dotclass: dotclass })
        } else {
          dotclass[i] = ''
          that.setData({ dotclass: dotclass })
        }
      }
      n++;
      if (n == 4) { clearInterval(timer) }
    }, 500)

  },
  onShareAppMessage: function () {

  }
})