// pages/orderDetail/orderDetail.js
var gConfig = getApp();
var util = require('../../utils/md5.js');
Page({
  data: {
    contactname: '联系方式',
    isonline: true,
    imgPath: gConfig.imgHttp
  },
  onLoad: function (options) {
    wx.showLoading({
      title: '加载中...',
    })
    // 页面初始化 options为页面跳转所带来的参数
    var that = this;
    var wxData = wx.getStorageSync('wxData')
    var isOpenPay = wxData.isOpenPay
    // var isOpenPay = 0;
    if (isOpenPay == 1) {
      if ((options.statuscode >= 10 && options.statuscode <= 23)||options.status==1) {
        that.setData({
          ispaid: false,
          ispaids: false
        })
      } else {
        that.setData({
          ispaid: true,
          ispaids: true
        })
      }
      that.setData({
        isPay: false,
        orderId: options.orderId,
        status: options.status,
        statuscode: options.statuscode
      })
    } else {
      that.setData({
        isPay: true,
        isonline: false,
        ispaids: false,
        ispaid: true,
        orderId: options.orderId
      })
    }
  },
  onReady: function () {
    // 页面渲染完成
  },
  onShow: function () {
   
    // 页面显示
    this.orderDetailFn();
  },
  onHide: function () {
    // 页面隐藏
  },
  onUnload: function () {
    // 页面关闭
  },
  //打电话接口
  phone: function (event) {
    var that = this;
    var orderDetailData = that.data.orderDetailData;
    var mob = orderDetailData.serviceTel;
    wx.makePhoneCall({
      phoneNumber: mob
    })
  },
  // shopDetailFn: function (event) {
  //   var that = this;
  //   var itemId = event.currentTarget.dataset.itemid;
  //   var skuId = event.currentTarget.dataset.skuid;
  //   var itemsData = that.data.itemsData;
  //   var statuscode = that.data.statuscode;
  //   for (var i = 0; i < itemsData.length; i++) {
  //     if (itemsData[i].skuId == skuId) {
  //       if (itemsData[i].isItem == 1) {
  //         if (statuscode.code == 200) {
  //           wx.redirectTo({
  //             url: '../shopDetail/shopDetail?itemId=' + itemId
  //           })
  //         } else {
  //           wx.showToast({
  //             title: '该商品不在本区域展示',
  //             icon: 'success',
  //             duration: 2000
  //           })
  //         }

  //       }
  //     }

  //   }

  // },
  orderDetailFn: function (event) {
    var that = this;
    var orderId = that.data.orderId;
    var sign = util.hexMD5('orderId=' + orderId + gConfig.key);
    wx.request({
      url: gConfig.http + 'xcx/v2/order/detail',
      data: { orderId: orderId, sign: sign },
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        wx.hideLoading()
        var orderDetailData = res.data.data;
        var statuscode = res.data.result
        var companyData = res.data.data.company;
        var itemsData = res.data.data.items;
        if (!orderDetailData.contact) {
          that.setData({
            contactname: orderDetailData.contact,
            isContact: false
          })
        } else {
          that.setData({
            contactname: '联系方式 :',
            isContact: true
          })
        }
        var gifts = [];
        for (var i = 0; i < itemsData.length; i++) {
          itemsData[i].price = itemsData[i].price.toFixed(2)
          itemsData[i].img = itemsData[i].img + '.220x220.' + itemsData[i].img.split('.')[1];
          if (itemsData[i].isItem == 2) {
            gifts.push(itemsData[i])
            itemsData[i].isShop = true
          }
        }
        if (gifts.length > 0) {
          that.setData({
            giftData: gifts,
            isGift: false
          })
        } else {
          that.setData({
            isGift: true
          })
        }
        that.setData({
          statuscode: statuscode,
          orderDetailData: orderDetailData,
          companyData: companyData,
          itemsData: itemsData,
        })
        var totalfee = (orderDetailData.amount + orderDetailData.feeAmount - orderDetailData.couponDiscount - orderDetailData.discount).toFixed(2);
        if (orderDetailData.amount < orderDetailData.couponDiscount && orderDetailData.feeAmount != 0) {
          that.setData({
            shopprice: (orderDetailData.amount).toFixed(2),
            foldingfee: (orderDetailData.couponDiscount).toFixed(2),
            freightfee: (orderDetailData.feeAmount).toFixed(2),
            cutfee: (orderDetailData.discount).toFixed(2),
            totalfee: (orderDetailData.feeAmount).toFixed(2),
            timeStamp: orderDetailData.addedTime,
            integral: (orderDetailData.getPoints).toFixed(2)
          })
        } else if (orderDetailData.amount > orderDetailData.couponDiscount) {
          that.setData({
            shopprice: (orderDetailData.amount).toFixed(2),
            cutfee: (orderDetailData.couponDiscount).toFixed(2),
            freightfee: (orderDetailData.feeAmount).toFixed(2),
            foldingfee: (orderDetailData.discount).toFixed(2),
            totalfee: totalfee,
            timeStamp: orderDetailData.addedTime,
            integral: (orderDetailData.getPoints).toFixed(2)
          })
        } else if (orderDetailData.amount < orderDetailData.couponDiscount && orderDetailData.feeAmount == 0) {
          that.setData({
            shopprice: (orderDetailData.amount).toFixed(2),
            cutfee: (orderDetailData.couponDiscount).toFixed(2),
            freightfee: (orderDetailData.feeAmount).toFixed(2),
            foldingfee: (orderDetailData.discount).toFixed(2),
            totalfee: '0.00',
            timeStamp: orderDetailData.addedTime,
            integral: (orderDetailData.getPoints).toFixed(2)
          })
        }


      }
    })
  },
  //取消订单
  cancleFn: function (event) {
    var orderId = event.currentTarget.dataset.orderid;
    var sign = util.hexMD5('id=' + orderId + gConfig.key);
    wx.showModal({
      title: '取消提示',
      content: '您确定要取消该订单吗？',
      success: function (res) {
        if (res.confirm) {
          /*--重新渲染--*/
          wx.request({
            url: gConfig.http + 'xcx/order/del',
            data: {
              id: orderId,
              sign: sign
            },
            header: {
              'content-type': 'application/json'
            },
            success: function (res) {
              wx.showToast({
                title: '取消成功',
                icon: 'success',
                duration: 1000
              })
              setTimeout(function () {
                wx.switchTab({
                  url: '../index/index'
                })
              }, 1500)
            }
          })

        }
      }
    })
  },
  placeOrderFn: function (event) {
    // 下单方法
    var that = this;
    var companyId = wx.getStorageSync("sellData").companyId;
    var orderId = event.currentTarget.dataset.orderid;
    var sign = util.hexMD5('orderId=' + orderId + '&companyId=' + companyId + gConfig.key);
    wx.request({
      url: gConfig.http + 'xcx/v2/wx/prepardId',
      data: {
        orderId: orderId,
        companyId: companyId,
        sign: sign
      },
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        // 微信支付接口
        wx.requestPayment({
          'timeStamp': res.data.data.timeStamp,
          'nonceStr': res.data.data.nonceStr,
          'package': res.data.data.package,
          'signType': 'MD5',
          'paySign': res.data.data.paySign,
          'success': function (res) {
            wx.switchTab({
              url: '../index/index'
            })
          }
        })
        // 微信支付接口
      }
    })
  },
  onShareAppMessage: function () {

  }
})
