App({
  onLaunch: function () {
    wx.clearStorage();
    var that = this;
    var util = require('utils/md5.js');
    
    wx.login({
      success: function (res) {
        var sign = util.hexMD5('appId=' + that.appId +'&code=' + res.code + '&companyId=' + that.companyId + that.key);
        if (res.code) {
          wx.request({
            url: that.http + 'xcx/v2/common/login',
            data: { 
              appId: that.appId,
              code: res.code,
              companyId: that.companyId,
              sign: sign
            },
            header: { 'content-type': 'application/json' },
            success: function (res) {
              console.log(res)
              var wxData = {
                "wxOpenid": res.data.data.wxOpenid ,
                "clientId": res.data.data.clientId,
                "isOpenPay": res.data.data.isOpenPay,
                "region": res.data.data.region,
                "mob": res.data.data.mob,
                "name": res.data.data.name,
                "companyId": that.companyId
              }
              wx.setStorageSync('wxData', wxData);
              wx.setStorageSync('shoppingcarData', []);
            },
            complete: function (res){
              // console.log(res)
            }
          })
        } else {
          console.log('获取用户登录态失败！' + res.errMsg)
        }
      }
    })
  },
  companyId: 8,
  // companyId: 10011179,
  // companyId: 10000036,

  // http: "https://lz.51test.com/farms-msi/",
  // imgHttp: "http://192.168.0.116:8070/",
   
  http: "https://msi-mall.51zhongzi.com/",
  imgHttp: "https://img.51zhongzi.com/",
  key: '&key=9da1ec1d11f0401968d52cab64df46d8',
  appId: "wx5427c8a3d579e4fc"

})
